---
layout: default
tags: [home]
---

## [Aktuelles]({{ "/Aktuelles" | prepend: site.baseurl }})

{% for post in site.posts limit:1 %}
  * {{ post.date | date: "%b %-d, %Y" }} [{{ post.title }}]({{ post.url | prepend: site.baseurl }})
{% endfor %}
{% assign size_of_posts = site.posts | size %}
{% if size_of_posts > 1 %}
  * [...]({{ "/Aktuelles" | prepend: site.baseurl }})
{% endif %}

## [Mailingliste]({{ "/Mailinglist" | prepend: site.baseurl }})

{% assign today = 'now' | date: "%Y-%m-%d" %}
{% for meeting in site.data.meetings %}
  {% assign next_meeting = meeting.date | date: "%Y-%m-%d" %}
  {% if next_meeting >= today %}
    {% assign next_meeting = meeting %}
    {% break %}
  {% endif %}
{% endfor %}

## [Treffen]({{ "/Treffen" | prepend: site.baseurl }})
* nächster Stammtisch: {{ next_meeting.date | date: "%Y-%m-%d,%t%H Uhr" }}: {{ next_meeting.location }}
